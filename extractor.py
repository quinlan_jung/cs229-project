#!/usr/bin/env python

# http://www.nltk.org/book/ch01.html
#nltk.download()
from nltk.corpus import stopwords
import os
import re
import csv
from bs4 import BeautifulSoup # http://www.crummy.com/software/BeautifulSoup/#Download


sample_folder = "sample_files"
mapping_file = os.path.join(sample_folder, "train_v2.csv")
training_files = os.path.join(sample_folder, "training_set") # Change to 'playground_training_set' for smaller dataset
test_files = os.path.join(sample_folder, "test_set") # Change to 'playground_test_set' for smaller dataset
stops = set(stopwords.words("english"))  # In Python, searching a set is much faster than searching a list, so convert the stop words to a set

def tokenize(html):
    soup = BeautifulSoup(html, "html.parser")

    # Remove javascript and style contents for now
    for elem in soup.findAll(['script', 'style']):
        elem.extract()

    # https://www.kaggle.com/c/word2vec-nlp-tutorial/details/part-1-for-beginners-bag-of-words
    texts = soup.get_text() # Remove HTML
    texts = re.sub("[^a-zA-Z]", " ", texts) # Remove non-letters
    texts = texts.lower().split() # Convert to lower case, split into individual words
    texts = [w for w in texts if not w in stops] # Remove stop words
    texts = " ".join(texts) #Join the words back into one string separated by space, and return the result.
    return texts
        
# Returns a map of all the file classifications: Map<File_Name, 0/1>
def createMap():
    with open(mapping_file, mode='r') as infile:
        reader = csv.reader(infile)
        next(reader)
        classifications = {rows[0]:int(rows[1]) for rows in reader}
        return classifications

# Returns the classification (1/0) of the file (123_raw_html.txt)
def getClassification(file_name):
    return classifications[file_name]

classifications = createMap() # Initialize our classifications map