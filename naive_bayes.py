import extractor as ex
import os
import copy
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from ficlearn.feature_extraction.text import BnsTransformer
import numpy as np
import itertools
import calendar
import time

# Parameters:
# path_to_dir = path we are iterating over
# func = a function of signature func(file_name, tokens, aMap)
# aMap = a regular dictionary
#
# Effects:
# For every file in the directory, call func(file_name, tokens, aMap)
# where file_name is the name of the file
#       tokens is a string of html words we want
#       aMap is the original dictionary we passed in
def forEachFile(files, path_to_dir, func, aMap):
    for file_in_dir in files:
        file_path = os.path.join(path_to_dir, file_in_dir)
        html = open(file_path, 'r').read()
        tokens = ex.tokenize(html)
        func(file_in_dir, tokens, aMap)

templateDict = {'predicted_target':[],
                'target':[],
                'data':[]}

#http://scikit-learn.org/stable/tutorial/text_analytics/working_with_text_data.html
def nbFunc(file_name, tokens, aMap):
    classification = ex.getClassification(file_name)
    aMap['target'].append(classification)
    aMap['data'].append(tokens)

# Perform k folds on n files
def performKFolds(n, k):
    assert n % k == 0 # For simplicity's sake
    print "KFolds: k = {}, n = {}".format(k, n)
    sectionSize = n/k
    allFiles = list(itertools.islice(os.listdir(ex.training_files), 0, n))

    testError = []
    trainError = []
    truePositive = []
    trueNegative = []
    falsePositive = []
    falseNegative = []
    for i in range(0, k):
        testStart = i * sectionSize
        testEnd = (i + 1) * sectionSize
        testFiles = list(itertools.islice(allFiles, testStart, testEnd))
        trainFiles = [item for item in allFiles if item not in testFiles]
        print "testStart {} testEnd {}".format(testStart, testEnd)
        print testFiles
        print trainFiles
        resTuple = performMultinomialNB(trainFiles, ex.training_files, testFiles, ex.training_files)
        testError.append(resTuple[0])
        trainError.append(resTuple[1])
        truePositive.append(resTuple[2])
        trueNegative.append(resTuple[3])
        falsePositive.append(resTuple[4])
        falseNegative.append(resTuple[5])

    csv = "{},{},{},{},{},{},{},{}".format(k, n, np.mean(testError), np.mean(trainError), np.mean(truePositive), np.mean(trueNegative), np.mean(falsePositive), np.mean(falseNegative))
    print csv
    return csv

def performMultinomialNB(trainingFiles, trainDir, testFiles, testDir):
    trainDict = copy.deepcopy(templateDict)
    forEachFile(trainingFiles, trainDir, nbFunc, trainDict) # populate trainDict

    #Create training matrix
    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', MultinomialNB()),])

    text_clf = text_clf.fit(trainDict['data'], trainDict['target'])

    testDict = copy.deepcopy(templateDict)
    forEachFile(testFiles, testDir, nbFunc, testDict) # populate testDict

    #Calculate test error
    testDict['predicted_target'] = text_clf.predict(testDict['data'])
    testError = np.mean(testDict['predicted_target'] != testDict['target'])

    #Calculate training error
    trainDict['predicted_target'] = text_clf.predict(trainDict['data'])
    trainError = np.mean(trainDict['predicted_target'] != trainDict['target'])

    truePositive = statistics(testDict['predicted_target'], testDict['target'], 1, True)
    trueNegative = statistics(testDict['predicted_target'], testDict['target'], 0, True)
    falseNegative = statistics(testDict['predicted_target'], testDict['target'], 1, False)
    falsePositive = statistics(testDict['predicted_target'], testDict['target'], 0, False)
    out.write("{},{},{},{},{},{}".format(testError, trainError,truePositive,trueNegative,falsePositive,falseNegative))
    return (testError, trainError,truePositive,trueNegative,falsePositive,falseNegative)

def vectorTweaks(trainDir, n, t):
    allFiles = list(itertools.islice(os.listdir(ex.training_files), 0, n+t))
    allFiles = [f for f in allFiles if f in ex.classifications]
    testFiles = list(itertools.islice(allFiles, 0, t))
    trainFiles = [item for item in allFiles if item not in testFiles]

    trainDict = copy.deepcopy(templateDict)
    forEachFile(trainFiles, trainDir, nbFunc, trainDict) # populate trainDict
    
    countVec = CountVectorizer(stop_words="english")
    countVec.fit_transform(trainDict['data'])
    Y = trainDict['target']

    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', BnsTransformer(y=Y, vocab=countVec.vocabulary_)),
                         ('clf', MultinomialNB()),])

    #countVec = CountVectorizer(stop_words="english")
    #X = countVec.fit_transform(trainDict['data'])
    #X_copy = copy.deepcopy(X)
    #Y = trainDict['target']

    #bns = BnsTransformer(y=Y, vocab=countVec.vocabulary_)
    #bns.fit(X_copy)
    #X_bns = bns.transform(X_copy)
    #tf_transformer = TfidfTransformer(use_idf=False).fit(X)
    #X_bns = tf_transformer.transform(X)
    #print X_bns

    #text_clf = MultinomialNB().fit(X_bns, trainDict['target'])


    text_clf = text_clf.fit(trainDict['data'], trainDict['target'])

    #parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
    #              'tfidf__use_idf': (True, False),
     #             'clf__alpha': (1e-2, 1e-3),
    #}
    #gs_clf = GridSearchCV(text_clf, parameters, n_jobs=-1)
    #gs_clf = gs_clf.fit(trainDict['data'], trainDict['target'])
    #best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
    #for param_name in sorted(parameters.keys()):
        #print("%s: %r" % (param_name, best_parameters[param_name]))

    #print score

    testDict = copy.deepcopy(templateDict)
    forEachFile(testFiles, trainDir, nbFunc, testDict) # populate testDict

    #Calculate test error
    testDict['predicted_target'] = text_clf.predict(testDict['data'])
    testError = np.mean(testDict['predicted_target'] != testDict['target'])

    #Calculate train error
    trainDict['predicted_target'] = text_clf.predict(trainDict['data'])
    trainError = np.mean(trainDict['predicted_target'] != trainDict['target'])

    truePositive = statistics(testDict['predicted_target'], testDict['target'], 1, True) / float(len(testDict['predicted_target']))
    trueNegative = statistics(testDict['predicted_target'], testDict['target'], 0, True) / float(len(testDict['predicted_target']))
    falseNegative = statistics(testDict['predicted_target'], testDict['target'], 1, False) / float(len(testDict['predicted_target']))
    falsePositive = statistics(testDict['predicted_target'], testDict['target'], 0, False) / float(len(testDict['predicted_target']))
    return "{},{},{},{},{},{},{}".format(n, testError, trainError, truePositive,trueNegative,falsePositive,falseNegative)

def topFeatures(trainDict, n):
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(trainDict['data'])
    indices = np.argsort(vectorizer.idf_)[::-1]
    features = vectorizer.get_feature_names()
    top_features = [features[i] for i in indices[:n]]
    print top_features
    return top_features


def statistics(predicted_target, target, expectedValue, op):
    count = 0
    for i, val in enumerate(target):
        if ((predicted_target[i] == target[i]) == op) and target[i] == expectedValue:
            count = count + 1
    return count

with open(str(calendar.timegm(time.gmtime()))+'submission.csv', 'w') as out:
    out.write("n, testError, trainError, truePositive,trueNegative,falsePositive,falseNegative\n")
    for i in range(1000, 11000, 1000):
        stuff = vectorTweaks(ex.training_files, i, i/10) + '\n'
        print stuff
        out.write(stuff)
        #break

    #for i in range(1000, 11000, 1000):
        #print i


#vectorTweaks(ex.training_files, 1800, 200)

#f = open("output.csv","w") #opens file with name of "test.txt"
#f.write("k, n, testError, trainError, truePositive,trueNegative,falsePositive,falseNegative\n")
#f.write(performKFolds(2000, 10)+"\n")
#for i in range (10, 110, 10):
    #f.write(performKFolds(i, 10)+"\n")
#f.close()

#performKFolds(10, 10)