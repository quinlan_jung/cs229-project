%this script creates a .mat file of all the csv files and links them to a
%dictionary
clear; clc; close all

%user inputs
endname = '_raw_html.txt.csv';
dictname = 'corncob_lowercase.csv';
outname = 'InputData.mat';

%import dictionary
dict = importdata(dictname);
nd = length(dict);

files = dir; %create a list of all files
nf = length(files);
csvfiles = cell(nf,1);
nen = length(endname);
label = zeros(nf,1);example = label;
count = 1;
appmat = zeros(nf,nd);
for k = 1:nf
    fname = files(k).name;
    nfn = length(fname);
    if nfn>nen
        if strcmp(endname,fname(nfn+1-nen:end))
            struct = importdata(fname);
            words = struct.textdata(2:end);
            label(count)=struct.data(1);
            example(count)=str2double(fname(1:nfn-nen));
            app = struct.data(2:end);
            %find indices of words in dictionary
            [tf, loc] = ismember(words,dict); 
            %update matrix
            appmat(count,loc(tf))=app(tf);
            count=count+1;
        end
    end
    disp([num2str(k/nf*100) '% done'])
end
%get rid of null entries
appmat(count:end,:)=[];
label(count:end)=[];
example(count:end)=[];
%trim dictionary
appmatold = appmat;
dictold = dict;
i = 1;
for k = 1:nd
    if  any(appmat(:,k)) 
        appmat(:,i)=appmatold(:,k);
        dict{i}=dictold{k};
        i=i+1;
    end
end
appmat(:,i:end)=[];
dict(i:end)=[];
nd = length(dict); 
save(outname,'appmat','label','example','dict');