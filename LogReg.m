%This script computes the logistic regression and evaluates the error
clear; clc; close all

%User inputs
fname = 'InputData.mat';
alpha = 0.001;
errormax = 1e-6; %termination condition

%load the data
load(fname)
X = appmat; X=sparse(X);
y = label;

%% Find the maximum liklihood estimate
m = length(X(:,1));
n = length(X(1,:));
X = [ones(m,1),X]; %done to include intercept
nfold = 10; %number of folds to exclude for test data;
mtest = floor(m/nfold); 
h = @(X,th) 1./(1+exp(-X*th)); %returns a vector of length m (sigmoid function)
egen = 0.0;
for kfold=1:nfold;
    itest = (kfold-1)*mtest+1:kfold*mtest;
    itrain = 1:m; itrain(itest)=[];
    Xtest = X(itest,:); ytest=y(itest);
    Xtrain = X(itrain,:); ytrain=y(itrain);
    mtrain = length(ytrain);

    th = zeros(n+1,1); %including intercept
    thold = th;
    error=1;
    %solve stochastic gradient ascent
    while error>errormax
        i = ceil(rand*mtrain); %pick random index
        th = th+alpha*(ytrain(i)-h(Xtrain(i,:),th))*Xtrain(i,:)';
        error=norm(th-thold)./norm(th);
        thold=th;
    end
    yhat=h(Xtest,th)>0.5;
    egen= egen + 1/mtest*sum(yhat~=ytest);
    disp([num2str((kfold)/nfold*100) '% done'])
end
%% Display stats
egen = egen/nfold;
disp(['Error =' num2str(egen)])
