#!/usr/bin/env python

# http://www.nltk.org/book/ch01.html
#nltk.download(); punkt
#This extractor extends Quinlan's code to create CSV files out of the training 
#and test data. The CSV files have the classification followed by a word 
#accompanied by the number of appearances.
import nltk 
import os
import csv
import re
import string
from bs4 import BeautifulSoup # http://www.crummy.com/software/BeautifulSoup/#Download

sample_folder = "sample_files"
mapping_file = os.path.join(sample_folder, "train_v2.csv") 
training_files = os.path.join(sample_folder, "training_set")
test_files = os.path.join(sample_folder, "test_set")

def tokenize(path_to_dir):
	# For each file in sample_files directory,
	# Grab their text and put them in a token array
    punctuations = list(string.punctuation)
    punctuations.append("...")
    punctuations.append("'s")
    files_in_dir = os.listdir(path_to_dir)
    files_in_dir = [i for i in files_in_dir if i.endswith(".txt")] 
    for file_in_dir in files_in_dir:
        classification = getMapping(file_in_dir)
        print "Classification: " + classification
        file_path = os.path.join(path_to_dir, file_in_dir)
        html = open(file_path, 'r').read()
        soup = BeautifulSoup(html)
        [s.extract() for s in soup(['style', 'script', '[document]', 'head', 'title'])]
        visible_text = soup.getText()
        token = nltk.word_tokenize(visible_text)
        token = [i.lower() for i in token if i not in punctuations] 
        token = [i for i in token if not any(c.isdigit() for c in i)]
        token = nltk.FreqDist(token)
        writer = csv.writer(open(file_in_dir + ".csv", 'wb'))
        writer.writerow(['LABEL', classification])
        for key, value in token.items():
            writer.writerow([key.encode('utf-8'), value])

# Returns a map of all the file classifications: Map<File_Name, 0/1>
def createMap():
	with open(mapping_file, mode='r') as infile:
	    reader = csv.reader(infile)
	    classifications = {rows[0]:rows[1] for rows in reader}
	    return classifications

# Returns the classification (1/0) of the file (123_raw_html.txt)
def getMapping(file_name):
	return classifications[file_name]

classifications = createMap()
tokenize(training_files)
tokenize(test_files)




